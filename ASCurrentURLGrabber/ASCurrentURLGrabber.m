//
//  ASCurrentURLGrabber.m
//	ASCurrentURLGrabber

//  Created by Austin Saucier on 2/15/2014
//  Copyright (c) 2014 Austin Saucier. All rights reserved.
//

#import "ASCurrentURLGrabber.h"

// The ASCurrentURLGrabber error domain.
NSString        *   const   kASCurrentURLGrabberErrorDomain                   = @"com.austinbsaucier.ASCurrentURLGrabber.ErrorDomain";

// The ASCurrentURLGrabber error that is displayed when the bundle ID was not set
NSString        *   const   kASCurrentURLGrabberErrorBundleIDNotSet           = @"The browser with bundle ID %@ is not running.";

// The ASCurrentURLGrabber error that is displayed when the AppleScript was not found
NSString        *   const   kASCurrentURLGrabberErrorScriptNotFound           = @"Failed to find script for %@.";

// The ASCurrentURLGrabber error that is displayed when the there isn't a browser running
NSString        *   const   kASCurrentURLGrabberErrorBrowserNotRunning        = @"Failed to find a browser to grab the URL from.";

// The ASCurrentURLGrabber error that is displayed when the AppleScript failed to execute
NSString        *   const   kDCOURLGrabberErrorScriptExecutionFailed    = @"Failed to run script for %@:\n%@.";

@interface DCOURLGrabber()

/* The bundle ID of the app that was activated last. */
@property (copy) NSString *lastActiveBundleID;

/* Registers whether the object is monitoring app switches. */
@property (assign, getter = isMonitoring) BOOL monitoring;

/* 
 * Bundle IDs of apps that have supported AppleScripts. 
 */
+ (NSArray *)supportedBundleIDs;

/* 
 * Fires each time the user switches apps. 
 *
 * @param notification The app switch notification. 
 */
- (void)appDidActivate:(NSNotification *)notification;

/* 
 * Returns a new error with the ASCurrentURLGrabber error domain.
 *
 * @param code The `ASCurrentURLGrabberErrorCode` that corresponds with the error.
 * @param message The localized description for the error.
 */
- (NSError *)errorForCode:(ASCurrentURLGrabberErrorCode)code withDescription:(NSString *)description;

@end

@implementation ASCurrentURLGrabber

#pragma mark - Class Methods

+ (NSArray *)supportedBundleIDs {
    static id supportedBundleIDs = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        supportedBundleIDs = [NSArray arrayWithObjects:
                              @"com.apple.Safari",
                              @"com.google.Chrome",
                              @"com.operasoftware.Opera",
                              @"org.mozilla.firefox",
                              nil];
    });
    return supportedBundleIDs;
}

#pragma mark - Overrides

- (void)dealloc {
    [self stopMonitoring];
}

#pragma mark - Utilities

- (NSError *)errorForCode:(ASCurrentURLGrabberErrorCode)code withDescription:(NSString *)description {
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          description, NSLocalizedDescriptionKey,
                          nil];
    return [[NSError alloc] initWithDomain:kASCurrentURLGrabberErrorDomain code:code userInfo:dict];
}

#pragma mark - Event Listeners

- (void)startMonitoring {
    if(self.isMonitoring) return;
    
    // Register for app switching
    self.monitoring = YES;
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self
                                                           selector:@selector(appDidActivate:)
                                                               name:NSWorkspaceDidActivateApplicationNotification
                                                             object:nil];
}

- (void)stopMonitoring {
    if(!self.monitoring) return;
    
    // Stop listening
    self.monitoring = NO;
    [[[NSWorkspace sharedWorkspace] notificationCenter] removeObserver:self name:NSWorkspaceDidActivateApplicationNotification object:nil];

}

- (void)appDidActivate:(NSNotification *)notification {
    NSRunningApplication *app = [notification.userInfo objectForKey:@"NSWorkspaceApplicationKey"];
    
    if([[ASCurrentURLGrabber supportedBundleIDs] containsObject:app.bundleIdentifier]) {
        // Set last activated app
        self.lastActiveBundleID = app.bundleIdentifier;
    }
}

#pragma mark - Actual URL Grabbing

- (NSURL *)grabURLFromBundleID:(NSString *)bundleID withError:(NSError *__autoreleasing *)error {

    // Check if AppleScript is available
    NSString *scriptPath = [[NSBundle mainBundle] pathForResource:self.lastActiveBundleID ofType:@"scpt"];
    NSDictionary *scriptLoadError;
    NSAppleScript *script = [[NSAppleScript alloc] initWithContentsOfURL:[NSURL fileURLWithPath:scriptPath] error:&scriptLoadError];
    
    if(scriptLoadError) {
        *error = [self errorForCode:ASCurrentURLGrabberErrorCodeScriptNotFound withDescription:kASCurrentURLGrabberErrorScriptNotFound];
        
        return nil;
    }
    
    // Check if app with bundle ID is running
    if([NSRunningApplication runningApplicationsWithBundleIdentifier:self.lastActiveBundleID].count < 1) {
        *error = [self errorForCode:ASCurrentURLGrabberErrorCodeBrowserNotRunning withDescription:kASCurrentURLGrabberErrorBrowserNotRunning];
        
        return nil;
    }
    
    // Grab URL using AppleScript
    NSDictionary *scriptExecuteError;
    NSAppleEventDescriptor *result = [script executeAndReturnError:&scriptExecuteError];
    if(scriptExecuteError) {
        
        *error = [self errorForCode:ASCurrentURLGrabberErrorCodeScriptExecutionFailed withDescription:[NSString stringWithFormat:kASCurrentURLGrabberErrorScriptExecutionFailed, self.lastActiveBundleID, scriptExecuteError]];
        
        return nil;
    }
    
    // Return what we got
    return [[NSURL alloc] initWithString:result.stringValue];
}

- (NSURL *)grabURLWithError:(NSError *__autoreleasing *)error {
    if(!self.lastActiveBundleID) { // No compatible app was switched to, or the bundle hasn't been set
        *error = [self errorForCode:ASCurrentURLGrabberErrorCodeBundleIDNotSet withDescription:kASCurrentURLGrabberErrorBundleIDNotSet];
        return nil;
    }
    
    return [self grabURLFromBundleID:self.lastActiveBundleID withError:error];
}

@end
