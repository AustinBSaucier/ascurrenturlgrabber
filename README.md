# ASCurrentURLGrabber

This handy Cocoa utility class uses AppleScripts to grab the URL from the currently active tab of the most recently used browser. It currently works with Safari and Chrome only, but it is trivial to add additional browser support.  

## Usage

1. Download or clone this repository.
2. Add both source files from the `ASCurrentURLGrabber` folder and the AppleScripts to your project.

### Grab the URL from a Specific Bundle ID

To retrieve the URL from a particular browser (in this case, Safari), use the following:

	ASCurrentURLGrabber *grabber = [[ASCurrentURLGrabber alloc] init];
    NSURL *url = [grabber grabURLFromBundleID:@"com.apple.Safari" withError:&grabError];
    if (grabError) {
        NSLog(@"Failed to retrieve URL: %@", grabError);
    } else {
        NSLog(@"Got URL: %@", url.absoluteString);
    }
    
### Grab URL by Monitoring App Switches

First, start monitoring like this:

	ASCurrentURLGrabber *grabber = [[ASCurrentURLGrabber alloc] init];
    [grabber startMonitoring];
    
Then, simply grab the URL of the currently active tab like so:

	NSURL *url = [grabber grabURLWithError:&grabError];
    if (grabError) {
        NSLog(@"Failed to retrieve URL: %@", grabError);
    } else {
        NSLog(@"Got URL: %@", url.absoluteString);
    }
    
## License

MIT License. See `LICENSE` for details.